package com.ac.androweb.Auth.infrastructure.adapters.output;

import com.ac.androweb.Auth.application.port.output.AuthPersistencePort;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import com.ac.androweb.User.infrastruture.adapters.output.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthPersistenceAdapter implements AuthPersistencePort {
    private final UserRepository userRepository;

    @Override
    public UserEntity saveUserWhitRoles(UserEntity user) {
        return userRepository.save(user);
    }
}
