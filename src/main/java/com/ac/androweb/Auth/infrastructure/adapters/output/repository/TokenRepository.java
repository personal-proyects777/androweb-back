package com.ac.androweb.Auth.infrastructure.adapters.output.repository;

import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface TokenRepository extends JpaRepository<TokenEntity, Long> {
    @Query(value = """
                select t from TokenEntity t
                inner join UserEntity user on user.id = t.user.id
                where user.id = :id and (t.expired = false and t.revoked = false)
            """)
    Set<TokenEntity> findAllValidTokenByUser(Long id);

    Optional<TokenEntity> findByToken(String token);
}
