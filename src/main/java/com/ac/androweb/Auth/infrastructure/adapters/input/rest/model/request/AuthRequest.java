package com.ac.androweb.Auth.infrastructure.adapters.input.rest.model.request;

import com.ac.androweb.User.infrastruture.adapters.input.rest.model.request.UserCreateRequest;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AuthRequest extends UserCreateRequest {
    private String token;
}
