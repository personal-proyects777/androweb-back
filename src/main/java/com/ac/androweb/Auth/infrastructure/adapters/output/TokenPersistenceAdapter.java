package com.ac.androweb.Auth.infrastructure.adapters.output;

import com.ac.androweb.Auth.application.port.output.TokenPersistencePort;
import com.ac.androweb.Auth.domain.model.Token;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;
import com.ac.androweb.Auth.infrastructure.adapters.output.mapper.TokenPersistenceMapper;
import com.ac.androweb.Auth.infrastructure.adapters.output.repository.TokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

@Component
@AllArgsConstructor
public class TokenPersistenceAdapter implements TokenPersistencePort {

    private final TokenRepository tokenRepository;
    private final TokenPersistenceMapper tokenPersistenceMapper;

    @Override
    public Set<Token> findAllValidTokenByUser(Long id) {
        return Set.of();
    }

    @Override
    public Token save(Token token) {
        return tokenPersistenceMapper.toToken(tokenRepository.save(tokenPersistenceMapper.toTokenEntity(token)));
    }

    @Override
    public TokenEntity saveWhitUser(TokenEntity tokenEntity) {
        return tokenRepository.save(tokenEntity);
    }

    @Override
    public Optional<Token> findByToken(String token) {
        return tokenRepository.findByToken(token)
                .map(tokenPersistenceMapper::toToken);
    }

    @Override
    public Optional<Token> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) {

    }
}
