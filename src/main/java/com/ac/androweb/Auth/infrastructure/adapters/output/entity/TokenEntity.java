package com.ac.androweb.Auth.infrastructure.adapters.output.entity;

import com.ac.androweb.Auth.domain.model.TokenType;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tokens")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenEntity {
    @Enumerated(EnumType.STRING)
    public TokenType tokenType = TokenType.BEARER;
    public boolean revoked;
    public boolean expired;
    @ManyToOne
    @JoinColumn(name = "user_id")
    public UserEntity user;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Lob
    @Column(length = 1000, unique = true)
    private String token;

}
