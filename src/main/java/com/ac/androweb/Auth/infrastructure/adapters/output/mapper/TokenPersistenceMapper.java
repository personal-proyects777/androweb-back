package com.ac.androweb.Auth.infrastructure.adapters.output.mapper;

import com.ac.androweb.Auth.domain.model.Token;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface TokenPersistenceMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    TokenEntity toTokenEntity(Token token);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    Token toToken(TokenEntity tokenEntity);
}
