package com.ac.androweb.Auth.infrastructure.adapters.input.rest.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LoginRequest {
    @NotBlank(message = "Field username cannot be empty or null")
    private String username;
    @NotBlank(message = "Field password cannot be empty or null")
    private String password;
}
