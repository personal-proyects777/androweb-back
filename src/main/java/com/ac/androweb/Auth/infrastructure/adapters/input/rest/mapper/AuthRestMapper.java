package com.ac.androweb.Auth.infrastructure.adapters.input.rest.mapper;

import com.ac.androweb.Auth.infrastructure.adapters.input.rest.model.request.AuthRequest;
import com.ac.androweb.Auth.infrastructure.adapters.input.rest.model.response.AuhtResponse;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface AuthRestMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserEntity toUserEntity(AuthRequest authRequest);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    AuhtResponse toAuhtResponse(AuthRequest userEntity);
}
