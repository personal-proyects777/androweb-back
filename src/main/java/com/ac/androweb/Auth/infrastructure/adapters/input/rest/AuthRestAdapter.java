package com.ac.androweb.Auth.infrastructure.adapters.input.rest;

import com.ac.androweb.Auth.application.port.input.AuthServicePort;
import com.ac.androweb.Auth.application.port.input.TokenServicePort;
import com.ac.androweb.Auth.domain.model.RoleEnum;
import com.ac.androweb.Auth.domain.model.TokenType;
import com.ac.androweb.Auth.infrastructure.adapters.input.rest.mapper.AuthRestMapper;
import com.ac.androweb.Auth.infrastructure.adapters.input.rest.model.request.AuthRequest;
import com.ac.androweb.Auth.infrastructure.adapters.input.rest.model.response.AuhtResponse;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.PermissionEntity;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.RoleEntity;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;
import com.ac.androweb.Auth.infrastructure.adapters.output.mapper.TokenPersistenceMapper;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/auth")
@AllArgsConstructor
public class AuthRestAdapter {

    private final AuthServicePort userServicePort;
    private final TokenServicePort tokenServicePort;
    private final AuthRestMapper authRestMapper;
    private final TokenPersistenceMapper tokenPersistenceMapper;

    @PostMapping("/register")
    public ResponseEntity<AuhtResponse> register(@Valid @RequestBody AuthRequest authRequest) {

        UserEntity userEntityRequest = authRestMapper.toUserEntity(authRequest);
        userEntityRequest.setPassword("1234");

        PermissionEntity read = new PermissionEntity();
        read.setName("READ");

        PermissionEntity create = new PermissionEntity();
        create.setName("CREATE");

        PermissionEntity delete = new PermissionEntity();
        delete.setName("DELETE");


        RoleEntity rolDeveloper = new RoleEntity();
        rolDeveloper.setRoleEnum(RoleEnum.DEVELOPER);
        rolDeveloper.setPermissionList(Set.of(read, create, delete));

        userEntityRequest.setRoles(Set.of(rolDeveloper));


        // Generamos el token
        Map<String, Object> example = new HashMap<>();
        String token = tokenServicePort.generateToken(example, userEntityRequest);
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setTokenType(TokenType.BEARER);
        tokenEntity.setToken(token);
        tokenEntity.setUser(userEntityRequest);
        // userEntityRequest.setTokens(Set.of(tokenEntity));


        userServicePort.registerUser(userEntityRequest);
        tokenServicePort.save(tokenEntity);

        authRequest.setToken(token);
        System.out.println("Token: " + token);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(authRestMapper.toAuhtResponse(authRequest));
    }
}
