package com.ac.androweb.Auth.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role {
    private Long id;
    private RoleEnum roleEnum;
}
