package com.ac.androweb.Auth.domain.model;

public enum RoleEnum {
    ADMIN,
    USER,
    INVITED,
    DEVELOPER
}
