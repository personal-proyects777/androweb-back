package com.ac.androweb.Auth.domain.model;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Token {
    public TokenType tokenType = TokenType.BEARER;
    public boolean revoked;
    public boolean expired;
    private Long id;
    private String token;
}
