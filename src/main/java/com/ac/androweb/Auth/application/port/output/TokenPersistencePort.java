package com.ac.androweb.Auth.application.port.output;

import com.ac.androweb.Auth.domain.model.Token;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;

import java.util.Optional;
import java.util.Set;

public interface TokenPersistencePort {
    Set<Token> findAllValidTokenByUser(Long id);

    Token save(Token token);

    TokenEntity saveWhitUser(TokenEntity tokenEntity);

    Optional<Token> findByToken(String token);

    Optional<Token> findById(Long id);

    void deleteById(Long id);

    // Aqui deben estar los metodos para generar el token y se deben implementar en la clase userService
    // en el metodo save
}
