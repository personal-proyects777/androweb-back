package com.ac.androweb.Auth.application.port.input;

import com.ac.androweb.Auth.domain.model.Token;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.Map;
import java.util.function.Function;

public interface TokenServicePort {
    Token findByToken(String token);

    Token findById(Long id);

    TokenEntity save(TokenEntity token);

    Token update(Long id, Token token);

    void delete(Long id);

    // AUTH TOKEN
    String generateToken(Map<String, Object> extraClaims, UserDetails userDetails);

    SecretKey generateSecretedKey(String secrectKey);

    String buildToken(Map<String, Object> extraClaims, UserDetails userDetails, long expiration);

    Claims extractAllClaims(String token);

    <T> T extractClaim(String token, Function<Claims, T> claimsResolver);

    Date extractExpiration(String token);

    boolean isTokenExpired(String token);

    String extractUsername(String token);

    String extractAuthorities(String token);

    boolean isTokenValid(String token, UserDetails userDetails);

    String generateRefreshToken(UserDetails userDetails);
}
