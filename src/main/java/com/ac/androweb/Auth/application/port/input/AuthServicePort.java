package com.ac.androweb.Auth.application.port.input;

import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;

public interface AuthServicePort {
    UserEntity registerUser(UserEntity user);
}
