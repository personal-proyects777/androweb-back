package com.ac.androweb.Auth.application.services;

import com.ac.androweb.Auth.application.port.input.AuthServicePort;
import com.ac.androweb.Auth.application.port.output.AuthPersistencePort;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthService implements AuthServicePort {

    private final AuthPersistencePort authPersistencePort;

    @Override
    public UserEntity registerUser(UserEntity user) {
        return authPersistencePort.saveUserWhitRoles(user);
    }
}
