package com.ac.androweb.Auth.application.port.output;

import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;

public interface AuthPersistencePort {
    UserEntity saveUserWhitRoles(UserEntity user);
}
