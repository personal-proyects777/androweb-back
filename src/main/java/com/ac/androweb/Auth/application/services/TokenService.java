package com.ac.androweb.Auth.application.services;

import com.ac.androweb.Auth.application.port.input.TokenServicePort;
import com.ac.androweb.Auth.application.port.output.TokenPersistencePort;
import com.ac.androweb.Auth.domain.exception.TokenNotFoundException;
import com.ac.androweb.Auth.domain.model.Token;
import com.ac.androweb.Auth.infrastructure.adapters.output.entity.TokenEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TokenService implements TokenServicePort {

    private final TokenPersistencePort persistence;
    private final long jwtExpiration = 1800000;
    private final long refreshExpiration = 0;
    private final String secretKey = "d418227f7d2e81ff37bb952b645ed2444baeb4e16e7913230ece8159842250d1";

    @Override
    public Token findByToken(String token) {
        return persistence.findByToken(token)
                .orElseThrow(TokenNotFoundException::new);
    }

    @Override
    public Token findById(Long id) {
        return persistence.findById(id)
                .orElseThrow(TokenNotFoundException::new);
    }

    @Override
    public TokenEntity save(TokenEntity token) {
        return persistence.saveWhitUser(token);
    }

    @Override
    public Token update(Long id, Token token) {
        return persistence.findById(id)
                .map(tokenBD -> {
                    tokenBD.setToken(token.getToken());
                    tokenBD.setRevoked(token.isRevoked());
                    tokenBD.setTokenType(token.tokenType);
                    return persistence.save(tokenBD);
                }).orElseThrow(TokenNotFoundException::new);
    }

    @Override
    public void delete(Long id) {
        if (persistence.findById(id).isEmpty()) {
            throw new TokenNotFoundException();
        }
        persistence.deleteById(id);
    }

    @Override
    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return buildToken(extraClaims, userDetails, jwtExpiration);
    }

    @Override
    public SecretKey generateSecretedKey(String secretKey) {
        byte[] decodedKey = secretKey.getBytes();
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");
    }

    @Override
    public String buildToken(Map<String, Object> extraClaims, UserDetails userDetails, long expiration) {
        String authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        SecretKey customKey = this.generateSecretedKey(secretKey);

        return Jwts.builder()
                .signWith(customKey, Jwts.SIG.HS256)
                .issuer("AUTH-BACKEND")
                .subject(userDetails.getUsername())
                .claim("authorities", authorities)
                .issuedAt(new Date())
                .expiration(new Date(System.currentTimeMillis() + expiration))
                .id(UUID.randomUUID().toString())
                .claims(extraClaims)
                .notBefore(new Date(System.currentTimeMillis()))
                .compact();
    }

    @Override
    public Claims extractAllClaims(String token) {
        SecretKey customKey = this.generateSecretedKey(secretKey);
        return Jwts
                .parser()
                .verifyWith(customKey)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

    @Override
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    @Override
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    @Override
    public boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    @Override
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    @Override
    public String extractAuthorities(String token) {
        return extractClaim(token, claims -> claims.get("authorities").toString());
    }

    @Override
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    @Override
    public String generateRefreshToken(UserDetails userDetails) {
        return buildToken(new HashMap<>(), userDetails, refreshExpiration);
    }
}
