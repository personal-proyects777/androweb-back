package com.ac.androweb.User.domain.model;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {

    private Long id;
    private String username;
    private String email;
    private String name;
    private String number;
}
