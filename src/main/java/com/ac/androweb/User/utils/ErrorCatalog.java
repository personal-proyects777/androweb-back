package com.ac.androweb.User.utils;

public enum ErrorCatalog {
    USER_NOT_FOUND("ERR_STUDENT_001", "User not found"),
    INVALID_USER("ERR_STUDENT_002", "Invalid User parameters"),
    GENERIC_ERROR("ERR_GENERIC_001", "An unexpected error occurred");


    private final String code;
    private final String message;

    ErrorCatalog(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
