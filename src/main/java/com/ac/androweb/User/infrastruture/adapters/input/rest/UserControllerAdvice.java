package com.ac.androweb.User.infrastruture.adapters.input.rest;

import com.ac.androweb.Errors.model.ErrorResponse;
import com.ac.androweb.User.domain.exception.UserNotFoundExection;
import com.ac.androweb.User.utils.ErrorCatalog;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.stream.Collectors;

import static com.ac.androweb.User.utils.ErrorCatalog.*;

@RestControllerAdvice
public class UserControllerAdvice {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserNotFoundExection.class)
    public ErrorResponse handUserNotFoundException() {
        ErrorResponse error = new ErrorResponse();
        error.setCode(USER_NOT_FOUND.getCode());
        error.setMessage(USER_NOT_FOUND.getMessage());
        error.setTime(LocalDateTime.now());
        return error;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handStudentInvalidStudentException(MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        ErrorResponse error = new ErrorResponse();
        error.setCode(INVALID_USER.getCode());
        error.setMessage(INVALID_USER.getMessage());
        error.setDetails(result.getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList())
        );
        error.setTime(LocalDateTime.now());

        return error;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleInvalidGenericError(Exception e) {

        ErrorResponse error = new ErrorResponse();
        error.setCode(GENERIC_ERROR.getCode());
        error.setMessage(GENERIC_ERROR.getMessage());
        error.setDetails(Collections.singletonList(e.getMessage()));
        error.setTime(LocalDateTime.now());

        return error;
    }
}
