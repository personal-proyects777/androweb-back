package com.ac.androweb.User.infrastruture.adapters.input.rest.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateRequest {

    @NotBlank(message = "Field username cannot be empty or null")
    private String username;
    @NotBlank(message = "Field email cannot be empty or null")
    private String email;
    @NotBlank(message = "Field name cannot be empty or null")
    private String name;
    @JsonProperty("number_phone")
    private String number = "";
}
