package com.ac.androweb.User.infrastruture.adapters.output;

import com.ac.androweb.User.application.ports.output.UserPersistencePort;
import com.ac.androweb.User.domain.model.User;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import com.ac.androweb.User.infrastruture.adapters.output.mapper.UserPersistenceMapper;
import com.ac.androweb.User.infrastruture.adapters.output.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class UserPersistenceAdapter implements UserPersistencePort {

    private final UserRepository repository;
    private final UserPersistenceMapper mapper;

    @Override
    public Optional<UserEntity> findByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public Optional<User> findById(Long id) {
        return repository.findById(id)
                .map(mapper::toUser);
    }

    @Override
    public List<User> findAll() {
        return mapper.toStudenList(repository.findAll());
    }

    @Override
    public User save(User user) {
        return mapper.toUser(repository.save(mapper.toUserEntity(user)));
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
