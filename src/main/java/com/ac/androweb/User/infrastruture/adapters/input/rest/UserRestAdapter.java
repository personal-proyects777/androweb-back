package com.ac.androweb.User.infrastruture.adapters.input.rest;

import com.ac.androweb.User.application.ports.input.UserServicePort;
import com.ac.androweb.User.infrastruture.adapters.input.rest.mapper.UserRestMapper;
import com.ac.androweb.User.infrastruture.adapters.input.rest.model.request.UserCreateRequest;
import com.ac.androweb.User.infrastruture.adapters.input.rest.model.response.UserResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@AllArgsConstructor
public class UserRestAdapter {

    private final UserServicePort servicePort;
    private final UserRestMapper restMapper;

    @GetMapping("")
    public List<UserResponse> findAll() {
        return restMapper.toUserResponseList(servicePort.findAll());
    }

    @GetMapping("/{id}")
    public UserResponse findById(@PathVariable Long id) {
        return restMapper.toUserResponse(servicePort.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<UserResponse> save(@Valid @RequestBody UserCreateRequest createRequest) {

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(restMapper.toUserResponse(servicePort.save(restMapper.toUser(createRequest))));
    }

    @PutMapping("/{id}")
    public UserResponse update(@PathVariable Long id, @Valid @RequestBody UserCreateRequest createRequest) {
        return restMapper.toUserResponse(servicePort.update(id, restMapper.toUser(createRequest)));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        servicePort.deleteById(id);
    }
}
