package com.ac.androweb.User.infrastruture.adapters.input.rest.mapper;

import com.ac.androweb.User.domain.model.User;
import com.ac.androweb.User.infrastruture.adapters.input.rest.model.request.UserCreateRequest;
import com.ac.androweb.User.infrastruture.adapters.input.rest.model.response.UserResponse;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserRestMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    User toUser(UserCreateRequest userCreateRequest);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserResponse toUserResponse(User user);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    List<UserResponse> toUserResponseList(List<User> userList);

}
