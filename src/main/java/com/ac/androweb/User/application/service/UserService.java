package com.ac.androweb.User.application.service;

import com.ac.androweb.User.application.ports.input.UserServicePort;
import com.ac.androweb.User.application.ports.output.UserPersistencePort;
import com.ac.androweb.User.domain.exception.UserNotFoundExection;
import com.ac.androweb.User.domain.model.User;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService implements UserServicePort {

    private final UserPersistencePort userPersistencePort;

    @Override
    public User findById(Long id) {
        return userPersistencePort.findById(id)
                .orElseThrow(UserNotFoundExection::new);
    }

    @Override
    public List<User> findAll() {
        return userPersistencePort.findAll();
    }

    @Override
    public User save(User user) {
        return userPersistencePort.save(user);
    }

    @Override
    public User update(Long id, User user) {
        return userPersistencePort.findById(id)
                .map(userBD -> {

                    userBD.setUsername(user.getUsername());
                    userBD.setEmail(user.getEmail());
                    userBD.setName(user.getName());
                    userBD.setNumber(user.getNumber());
                    return userPersistencePort.save(userBD);
                })
                .orElseThrow(UserNotFoundExection::new);
    }

    @Override
    public void deleteById(Long id) {
        if (userPersistencePort.findById(id).isEmpty()) {
            throw new UserNotFoundExection();
        }
        userPersistencePort.deleteById(id);
    }

    @Override
    public Optional<UserEntity> findByUsername(String username) {
        return userPersistencePort.findByUsername(username);
    }
}
