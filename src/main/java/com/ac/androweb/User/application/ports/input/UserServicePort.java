package com.ac.androweb.User.application.ports.input;

import com.ac.androweb.User.domain.model.User;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserServicePort {
    User findById(Long id);
    List<User> findAll();
    User save(User user);
    User update(Long id, User user);
    void deleteById(Long id);

    Optional<UserEntity> findByUsername(String username);
}
