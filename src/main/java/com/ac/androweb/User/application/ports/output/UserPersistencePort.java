package com.ac.androweb.User.application.ports.output;

import com.ac.androweb.User.domain.model.User;
import com.ac.androweb.User.infrastruture.adapters.output.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserPersistencePort {
    Optional<UserEntity> findByUsername(String username);
    Optional<User> findById(Long id);
    List<User> findAll();
    User save(User user);
    void deleteById(Long id);
}
