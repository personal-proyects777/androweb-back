package com.ac.androweb;

import com.ac.androweb.Auth.application.port.output.TokenPersistencePort;
import com.ac.androweb.User.infrastruture.adapters.output.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AcAndrowebBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcAndrowebBackApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(UserRepository userRepository, TokenPersistencePort persistence) {
		return args -> {
			/*PermissionEntity read = new PermissionEntity();
			read.setName("READ");

			PermissionEntity create = new PermissionEntity();
			create.setName("CREATE");

			PermissionEntity delete = new PermissionEntity();
			delete.setName("DELETE");

			RoleEntity rolAdmin = new RoleEntity();
			rolAdmin.setRoleEnum(RoleEnum.ADMIN);
			rolAdmin.setPermissionList(Set.of(read, create));

			RoleEntity rolUser = new RoleEntity();
			rolAdmin.setRoleEnum(RoleEnum.USER);

			RoleEntity rolDeveloper = new RoleEntity();
			rolDeveloper.setRoleEnum(RoleEnum.DEVELOPER);
			rolDeveloper.setPermissionList(Set.of(read, create, delete));


			UserEntity user1 = new UserEntity();
			user1.setName("Andres");
			user1.setUsername("andrus");
			user1.setEmail("andrus@");
			user1.setPassword("1234");
			user1.setRoles(Set.of(rolAdmin));

			UserEntity user2 = new UserEntity();
			user2.setName("Liz");
			user2.setUsername("Daniela");
			user2.setEmail("lizda");
			user2.setPassword("1234");
			user2.setRoles(Set.of(rolAdmin, rolDeveloper));


			userRepository.saveAll(List.of(user1, user2));

			TokenService service = new TokenService(persistence);
			Map<String, Object> example = new HashMap<>();

			String permissionsClaim = user2.getRoles()
					.stream()
					.flatMap(roleEntity -> roleEntity.getPermissionList().stream())
					.map(PermissionEntity::getName)
					.collect(Collectors.joining(","));
			example.put("permissions", permissionsClaim);

			String token = service.generateToken(example, user2);

			String token1 = service.generateRefreshToken(user2);
			System.out.println("token => " + token);

			System.out.println("token2 => " + token1);

			System.out.println("Username toke => " + service.extractUsername(token));

			System.out.println("isTokenExpired toke => " + service.isTokenExpired(token));

			System.out.println("isTokenValid toke => " + service.isTokenValid(token, user2));

			System.out.println("extractExpiration toke => " + service.extractExpiration(token));

			System.out.println("extractAuthorities toke => " + service.extractAuthorities(token));*/
		};
	}
}
